//Addition of two numbers
function addTwoNumbers(num1, num2) {
    const sum = num1 + num2;
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(sum);
}
addTwoNumbers(5, 15);

//Subtraction of two numbers
function subtractTwoNumbers(num1, num2) {
    const diff = num1 - num2;
    console.log("Displayed difference of " + num1 + " and " + num2);
    console.log(diff);
}
subtractTwoNumbers(20, 5);

//Multiplication of two numbers
function multiplyTwoNumbers(num1, num2) {
    const product = num1 * num2;
    console.log("Displayed product of " + num1 + " and " + num2);
    return product;
}
let productResult = multiplyTwoNumbers(50, 10);
console.log(productResult);

//Division of two numbers
function divideTwoNumbers(num1, num2) {
    const quotient = num1 / num2;
    console.log("Displayed quotient of " + num1 + " and " + num2);
    return quotient;
}
let quotientResult = divideTwoNumbers(50, 10);
console.log(quotientResult);

//Compute area of circle of a given radius
function areaOfCircle(radius) {
    console.log(
        "The result of getting the area of a circle with " + radius + " radius:"
    );
    let result = 3.14159 * radius * radius;
    return parseFloat(result.toFixed(2));
}
let radiusResult = areaOfCircle(15);
console.log(radiusResult);

//Compute average of 4 numbers
function getAverage(num1, num2, num3, num4) {
    const ave = (num1 + num2 + num3 + num4) / 4;
    console.log(
        "The average of " +
            num1 +
            ", " +
            num2 +
            ", " +
            num3 +
            " and " +
            num4 +
            ":"
    );
    return ave;
}
let aveResult = getAverage(20, 40, 60, 80);
console.log(aveResult);

//Compute for a passing or failing score
function getResult(score, total) {
    const ave = (score / total) * 100;

    console.log("Is " + score + "/" + total + " a passing score?");
    return ave >= 75;
}
let getPercentage = getResult(38, 50);
console.log(getPercentage);
